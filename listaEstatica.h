//
// Created by constantini on 28/03/2020.
//

#ifndef DATA_STRUCTURE_LISTAESTATICA_H
#define DATA_STRUCTURE_LISTAESTATICA_H

#endif //DATA_STRUCTURE_LISTAESTATICA_H

#include <iostream>
#include <string>

template <class type, int n>

struct Tlista {
    type lista[n];
    int counter;
    int size;
};


/*
   inicia
*/


template <class type, int size>
void Inicializa_Lista(Tlista <type, size> &lista) {
    lista.size = size;
    for(int i = 0; i < lista.size; i++) {
        lista.lista[i] = NULL;
    }

    lista.counter = 0;
}


/*
   adiciona no fim
*/


template <class type, int size>
bool Insere_Final_Lista(Tlista <type, size> &lista, type item) {
    if(lista.size == lista.counter) {
        throw "a lista esta cheia";
    }
    else {
        lista.lista[lista.counter] = item;
    }
    lista.counter++;
    return true;
}


/*
   adiciona no inicio
*/



template <class type, int size>
bool Insere_Inicio_Lista(Tlista <type, size> &lista, type item) {
    if(lista.size == lista.counter) {
        throw "a lista esta cheia";
    }

    for(int i = lista.counter; i >= 0; i--) {
        lista.lista[i + 1] = lista.list[i];
        std::cout << i << std::endl;
    }
    lista.list[0] = item;
    lista.counter++;
    return true;
}


/*
   remove do fim
*/



template <class type, int size>
bool Remove_Final_Lista(Tlista <type, size> &lista) {
    if(lista.counter == 0) {
        throw "a vazia";
    }
    else {
        lista.lista[lista.counter] = 0;
    }
    lista.counter--;
    return true;
}


/*
   remove do inicio
*/



template <class type, int size>
bool Remove_Inicio_Lista(Tlista <type, size> &lista) {
    if(lista.size == lista.counter) {
        throw "a lista esta cheia";
    }

    for(int i = 0; i < lista.counter; i++) {
        lista.list[i] = lista.list[i + 1];
    }
    lista.lista[lista.counter] = 0;
    lista.counter--;
    return true;
}



//using namespace std;
//
//int main()
//{
//    Tlist<int, 10> listo;
//    Inicializa_lista(listo);
//    push(listo, 50);
//    push(listo, 60);
//    unshift(listo, 20);
//    // pop(listo);
//    // shift(listo);
//    // cout << listo.list[0] << endl <<  listo.list[1] << endl << listo.list[2];
//
//    return 0;
//}